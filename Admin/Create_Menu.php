<?php
session_start();
if (isset($_SESSION['username'])) {
  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Food</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </head>

  <body style="background-color:lightgray">
    <div class="body" style="">
      <br>
      <h1 style="padding-left: 500px; ">HOME</h1></br>
      <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="margin-left: 40%;">
        <li class="nav-item">
          <a class="nav-link active" id="pills-create-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">CREATE</a>
        </li>
        <li class="nav-item" style="margin-left:100px">
          <a class="nav-link" id="pills-read-tab" href="Read_Menu.php" role="tab" aria-controls="pills-contact" aria-selected="false">READ</a>
        </li>
      </ul>
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">...</div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
      </div>
    </div>


    <div class="profile" style="position:fixed">
      <div style="background-color:lightslategray; width: 200px; height:765px; margin-top:-190px">
        <img src="../image/admiin.jpg" class="img-fluid" style="width:200px; height:100px; ">
        <h1><?php echo "Hello, " . $_SESSION['username'] ?></h1>
        <a href="logout.php"><button type="button" class="btn btn-secondary btn-lg" style="margin-top:500px; margin-left: 30px">LOG-OUT</button></a>
      </div>
    </div>
    <div class="create">
      <form style="background-color:lime; margin-left:300px; width:800px; height:550px; 
      position:absolute; padding-right: 80px; padding-left: 80px" enctype="multipart/form-data" method="POST" action="Create.php">

        <td><label for="exampleFormControlInput1">Nama Menu</label></td>
        <td>:</td>
        <td><input type="text" class="form-control" id="exampleFormControlInput1" name="nama"></td>


        <label for="exampleFormControlSelect1">jenis</label>
        <select class="form-control" id="exampleFormControlSelect1" name="jenis">
          <option>pembuka</option>
          <option>makanan besar</option>
          <option>cemilan</option>
          <option>minuman</option>
        </select>


        <label for="exampleFormControlInput1">kuantitas</label>
        <input type="number" class="form-control" id="exampleFormControlInput1" name="kuantitas">

        <label for="exampleFormControlInput1">harga</label>
        <input type="number" class="form-control" id="exampleFormControlInput1" name="harga">

        <label for="exampleFormControlInput1">gambar</label>
        <input type="file" class="form-control" id="exampleFormControlInput1" name="gambar" required>

        <label for="exampleFormControlTextarea1">deskripsi</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="deskripsi"></textarea>

        <br>
        
        <center><button type="submit" class="btn btn-primary btn-lg" name="submit">submit</button>
        <button type="reset" class="btn btn-secondary btn-lg" style="margin-left: 100px">reset</button></br></center>
      </form>
    </div>

  </body>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </html>
<?php
} else {
  ?>
  <script>
    alert("Anda Belom Login!");
    window.location.href = "Login_Admin.php";
  </script>
<?php
}
?>