<?php
session_start();
include "../Koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tampil Data Pelanggan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>

<div class="profile" style="position:fixed">
      <div style="background-color:lightslategray; width: 200px; height:755px; margin-top:0px">
        <img src="../image/admiin.jpg" class="img-fluid" style="width:200px; height:100px; ">
        <h1><?php echo "Hello, " . $_SESSION['username'] ?></h1>
        <a href="logout.php"><button type="button" class="btn btn-secondary btn-lg" style="margin-top:500px; margin-left: 30px">LOG-OUT</button></a>
      </div>
    </div>

    <div class="read" style="position:fixed; margin-left: 230px; margin-right: 100px; margin-top: 100px; background-color: lime;">
    <center>
        <h1>Data Pesanan</h1>
        <a href="Create_Menu.php">Tambah Menu</a>
        <br><br>
        <table class="table table-striped">
            <tr>
                <th scope="col">ID Menu</th>
                <th scope="col">Nama</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Gambar</th>
                <th scope="col">Kuantitas</th>
                <th scope="col">Jenis</th>
                <th scope="col">Harga</th>
                <th scope="col">Opsi</th>
            </tr>
            <?php
            $query = mysqli_query($koneksi, "SELECT * FROM menu") or die("Query Salah");
            foreach ($query as $data) {
            ?>
            <tr>
                <th><?php echo $data['id_menu']; ?></th>
                <td><?=$data['nama_menu'] ?></td>
                <td><?=$data['deskripsi_menu'] ?></td>
                <td><?=$data['gambar_menu'] ?></td>
                <td><?=$data['kuantitas'] ?></td>
                <td><?=$data['jenis_menu'] ?></td>
                <td><?=$data['harga_menu'] ?></td>
                <td><a href = "Edit_Menu.php?id_menu=<?=$data['id_menu']?>">Edit</a>
                <a href = "Hapus.php?id_menu=<?=$data['id_menu']?>">Hapus</td>
            </tr>
            <?php
            }
            
            ?>
        </table>
        </div>
    </center>
</body>
</html>