-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2019 at 02:33 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wad_kelas_tubes`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(50) NOT NULL,
  `id_menu` int(20) NOT NULL,
  `jumlah_pesanan` int(10) NOT NULL DEFAULT '0',
  `id_checkout` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id_checkout` int(20) NOT NULL,
  `no_meja` int(10) NOT NULL,
  `nama_pemesan` varchar(100) NOT NULL,
  `nohp` varchar(100) NOT NULL,
  `total_harga` bigint(100) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(50) NOT NULL,
  `nama_menu` varchar(100) NOT NULL,
  `deskripsi_menu` varchar(200) NOT NULL,
  `gambar_menu` varchar(100) NOT NULL,
  `kuantitas` int(50) NOT NULL,
  `jenis_menu` varchar(100) DEFAULT NULL,
  `harga_menu` bigint(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `deskripsi_menu`, `gambar_menu`, `kuantitas`, `jenis_menu`, `harga_menu`) VALUES
(3, 'Pudding Buah', 'Pudding  buah lewwwzat dengan berbagai rasa buah\r\nbuahan alami yang diambil dari petani langsung tanpa \r\nbahan pengawet', 'image/puding buah2.jpg', 21, 'pembuka', 15000),
(4, 'Lumpia', 'Lumpia yang terbuat dari bahan pilihan dengan isi bihun dan wortel dijamin bikin nagih!', 'image/lumpia.jpg', 15, 'pembuka', 18000),
(6, 'Indomie', 'Dengan cita rasa Indonesia banget, tentunya bikin kamu ketagihan!', 'image/Indomie.png', 45, 'makanan besar', 17000),
(8, 'Teh Manis', 'Bisa jadi pilihan kamu buat cari yang seger-seger nih guys!!', 'image/Teh_Manis.png', 70, 'minuman', 8000),
(9, 'Tahu Bulat', 'Cocok buat kamu yang suka nyemil tapi tetep dengan porsi yang pas', 'image/Tahu_Bulat.png', 30, 'cemilan', 12000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `id_menu` (`id_menu`),
  ADD KEY `id_checkout` (`id_checkout`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id_checkout`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id_checkout` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`id_checkout`) REFERENCES `checkout` (`id_checkout`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
